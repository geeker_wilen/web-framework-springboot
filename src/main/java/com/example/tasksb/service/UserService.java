package com.example.tasksb.service;

import com.example.tasksb.pojo.User;


public interface UserService {
    Message loginCheck(User user);
    boolean registerCheck(User user);

}
