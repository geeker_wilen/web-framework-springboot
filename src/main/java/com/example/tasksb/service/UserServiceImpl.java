package com.example.tasksb.service;

import com.example.tasksb.cache.FrozenAccountTool;
import com.example.tasksb.dao.UserMapper;
import com.example.tasksb.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;



    @Override
    public Message loginCheck(User user) {
        if (userMapper.getUserByUsername(user.getUsername())!=null){
            FrozenAccountTool login = FrozenAccountTool.init("login", userMapper.getUserByUsername(user.getUsername()).getId(), 3, 5);
            if(login.IsFrozen()){   return new Message("账户输入超过3次，请5分钟后重试",false); }
            if (user.getPassword().equals(userMapper.getUserByUsername(user.getUsername()).getPassword())){
                return new Message("登录成功",true);
            }
            System.out.println(login.GetFailCount());
            login.failCountIncrease();
        }
        return new Message("用户名和密码不正确",false);
    }

    @Override
    public boolean registerCheck(User u) {
        if (userMapper.getUserByUsername(u.getUsername()) == null){

            Map<String,Object> map = new HashMap<String,Object>();
            System.out.println(u.toString());
            map.put("username",u.getUsername());
            map.put("password",u.getPassword());
            userMapper.addUserByMap(map);
            return true;
        }
        return false;
    }
}
