package com.example.tasksb;

import com.example.tasksb.cache.FrozenAccountTool;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(value = "com.example.tasksb.*" )
@SpringBootApplication
public class TasksbApplication {

    public static void main(String[] args) {

        SpringApplication.run(TasksbApplication.class, args);
    }

}
