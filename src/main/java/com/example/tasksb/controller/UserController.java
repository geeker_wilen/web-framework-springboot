package com.example.tasksb.controller;

import com.example.tasksb.pojo.User;
import com.example.tasksb.service.Message;
import com.example.tasksb.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;

@Controller
public class UserController {
    @Autowired
    private UserServiceImpl userService;

    @GetMapping("/")
    public String index(){
        return "index";
    }

    @RequestMapping(path = "/login")
    public String login(){
        return "login";
    }

    /**
     * 请求参数绑定:
     *      把参数数据封装到JavaBean的类中
     *      默认把参数封装到函数的形参当中
     * @param user
     * @return
     */
    @RequestMapping(value = "/loginCheck")
    public ModelAndView loginCheck(ModelAndView mv,User user){
        Message message = userService.loginCheck(user);
        System.out.println("message:"+message.isTof()+" "+ message.getMessage());
        if (message.isTof() == true){
            return new ModelAndView("success");
        }
        mv.setViewName("login");
        mv.addObject("message",message.getMessage());
        return mv;
    }

    @RequestMapping(path = "/register")
    public String register(){
        return "register";
    }

    @RequestMapping(path = "registerCheck")
    public String registerCheck(User user) throws ParseException {
        boolean flag = userService.registerCheck(user);
        if (flag == true){
            return "registersuccess";
        }
        return "register";
    }
}
