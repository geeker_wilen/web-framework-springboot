package com.example.tasksb.pojo;

public class User {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int id;
    public String username;
    public String password;
    public String name;
    public int error=0;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }
}
