package com.example.tasksb.dao;

import com.example.tasksb.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface UserMapper {
    //根据username查询用户
    User getUserByUsername(String name);

    //通过Map去添加用户
    int addUserByMap(Map<String,Object> map);
}
